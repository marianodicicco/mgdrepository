﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsolePlayerMain
{
    class PlayState
    {
        public static bool gameLoop;
        private static bool pause;

        public void gameStart()
        {

            MenuState menu = new MenuState();
            Player player = new Player();
            Enemy[] enemies = new Enemy[10];
            Random rand = new Random();

            //ADDING ENEMIES TO ARRAY
            for (int i = 0; i < enemies.Length; i++)
            {
                enemies[i] = new Enemy(rand.Next(0, Console.WindowWidth), rand.Next(0, Console.WindowHeight));
                while (enemies[i].getX() == player.getX() && enemies[i].getY() == player.getY())
                {
                    enemies[i].setX(rand.Next(0, Console.WindowWidth) / 2);
                    enemies[i].setY(rand.Next(0, Console.WindowHeight) / 2);
                }
            }
            //----------------

            ConsoleKeyInfo keyInfo;

            gameLoop = false; //if true starts game
            pause = false; // sets false to pause

            //-------------------@@@@@@@@@@@@@@@@@@
            //menuState loop begins
            while (menu.menuLoop)
            {
                //Menu Inputs Start
                if (Console.KeyAvailable)
                {

                    keyInfo = Console.ReadKey();

                    if (!pause)
                    {
                        switch (keyInfo.Key)
                        {
                            /*case ConsoleKey.RightArrow:
                                player.moveRight();
                                break;
                            case ConsoleKey.LeftArrow:
                                player.moveLeft();
                                break;*/
                            case ConsoleKey.UpArrow:
                                menu.moveUp();
                                break;
                            case ConsoleKey.DownArrow:
                                menu.moveDown();
                                break;
                            case ConsoleKey.Spacebar:
                                menu.checkOption();
                                break;
                        }
                    }
                }
                //-----------------------


                //START DRAWING

                Console.Clear();
                menu.showMenu();
                //END DRAWING

                System.Threading.Thread.Sleep(200);


            }// menuloop ends

            //-------------------@@@@@@@@@@@@@@@@@@

            //gameloop begins
            while (gameLoop)
            {
                //Collision check
                for (int i = 0; i < enemies.Length; i++)
                {

                    if (enemies[i].getX() == player.getX() && enemies[i].getY() == player.getY())
                    {
                        player.decreaseHp(1);
                        player.moveToRespawn();
                        Console.Beep(350, 100);
                        Console.Beep(200, 50); ;
                    }

                    if (player.getHp() <= 0)
                    {
                        gameOver();
                    }

                }

                //Moving Enemies
                if (!pause)
                {
                    for (int i = 0; i < enemies.Length / 2; i++)
                    {
                        enemies[i].moveRightDown();
                    }

                    for (int i = 5; i < enemies.Length; i++)
                    {
                        enemies[i].moveLeftDown();
                    }
                }

                //---------------------------
                pause = player.Move(pause);
                //Character Inputs Start
                /*if (Console.KeyAvailable)
                {

                    keyInfo = Console.ReadKey();

                    if (!pause)
                    {
                        switch (keyInfo.Key)
                        {
                            case ConsoleKey.RightArrow:
                                player.moveRight();
                                break;
                            case ConsoleKey.LeftArrow:
                                player.moveLeft();
                                break;
                            case ConsoleKey.UpArrow:
                                player.moveUp();
                                break;
                            case ConsoleKey.DownArrow:
                                player.moveDown();
                                break;
                            case ConsoleKey.Spacebar:
                                doPause();
                                Console.SetCursorPosition(player.getX(), player.getY());
                                break;
                        }
                    }
                    else
                    {
                        if (keyInfo.Key == ConsoleKey.Spacebar)
                        {
                            pause = false;
                            Console.Beep(500, 100);
                        }
                    }
                }*/
                //-----------------------


                //START DRAWING
                player.draw();
                for (int i = 0; i < enemies.Length; i++)
                {
                    enemies[i].draw();
                }
                if (pause)
                {
                    Console.SetCursorPosition(0, 0);
                    Console.WriteLine("  ___   __   _  _  ____    ____   __   _  _   ____  ____  ____ ");
                    Console.WriteLine(" / __) / _) ( )/ )(  __)  (  _ ) / _) / )( ) / ___)(  __)(    ) ");
                    Console.WriteLine("( (_ )/    )/ )/ ) ) _)    ) __//    )) )/ ()___ ) ) _)  ) D (  ");
                    Console.WriteLine(" (___/)_/)_/)_)(_/(____)  (__)  (_/(_/)____/(____/(____)(____/  ");
                }

                //END DRAWING

                System.Threading.Thread.Sleep(200);


            }//gameloop ends

            Console.ReadKey();

        }

        private static void doPause()
        {
            Console.Beep(500, 100);
            Console.Beep(1000, 100);
            pause = true;
        }

        private static void gameOver()
        {
            gameLoop = false;
            Console.Beep(150, 100);
            Console.Beep(100, 100);
            Console.Beep(50, 100);
            Console.SetCursorPosition(0, 0);
            Console.WriteLine("  ___   __   _  _  ____     __   _  _  ____  ____ ");
            Console.WriteLine(" / __) / _) ( )/ )(  __)   /  ) / )( )(  __)(  _ ) ");
            Console.WriteLine("( (_ )/    )/ )/ ) ) _)   (  O )) )/ / ) _)  )  / ");
            Console.WriteLine(" (___/)_/)_/)_)(_/(____)   (__)  (__/ (____)(__)_)  ");

        }
    }
}

