﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsolePlayerMain
{
    class MenuState
    {
        private int option;
        private int maxOptions;
        public bool menuLoop;

        public MenuState()
        {
            menuLoop = true;
            maxOptions = 2;
        }

        public void showMenu()
        {
            showStartGame();
            showQuit();
            if (option == 0)
            {
                Console.SetCursorPosition(60, 2);
                Console.WriteLine("<-----");
                Console.SetCursorPosition(60, 3);
                Console.WriteLine("<-----");
            }
            else if (option == 1)
            {
                Console.SetCursorPosition(30, 5);
                Console.WriteLine("<-----");
                Console.SetCursorPosition(30, 6);
                Console.WriteLine("<-----");
            }
        }

        private void showQuit()
        {
            Console.WriteLine("  __   _  _  __  ____ ");
            Console.WriteLine(" /  ) / )( )(  )(_  _)");
            Console.WriteLine("(  O )) )/ ( )(   )(  ");
            Console.WriteLine(" (__) (____/(__) (__) ");
        }

        private void showStartGame()
        {
            Console.WriteLine(" ____  ____  __   ____  ____     ___   __   _  _  ____ ");
            Console.WriteLine("/ ___)(_  _)/ _) (  _ )(_  _)   / __) / _) ( )/ )(  __)");
            Console.WriteLine(")___ )  )( /    ) )   /  )(    ( (_ )/    )/ )/ ) ) _) ");
            Console.WriteLine("(____/ (__)(_/)_/(__)_) (__)    (___/)_/)_/(_)(_/(____)");
        }

        public void moveUp()
        {
            if (option > 0)
            {
                option--;
            }
        }

        public void moveDown()
        {
            if (option < maxOptions - 1)
            {
                option++;
            }
        }

        public void checkOption()
        {
            switch (option)
            {
                case 0:
                    menuLoop = false;
                    PlayState.gameLoop = true;
                    break;
                case 1:
                    menuLoop = false;
                    break;

            }
        }
    }
}
