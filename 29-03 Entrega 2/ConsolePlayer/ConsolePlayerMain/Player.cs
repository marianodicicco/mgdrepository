﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsolePlayerMain
{
    class Player
    {
        private int x;
        private int y;
        private int hp;

        public Player()
        {
            x = Console.WindowWidth / 2; // Horizontal Axis
            y = Console.WindowHeight / 2; // Vertical Axis
            hp = 1;
        }

        public int getX()
        {
            return x;
        }

        public int getY()
        {
            return y;
        }

        public void setX(int _x)
        {
            if (_x > 0 && _x < Console.WindowWidth - 1)
            {
                x = _x;
            }
        }

        public void setY(int _y)
        {
            if (_y > 0 && _y < Console.WindowHeight - 1)
            {
                x = _y;
            }
        }
        public virtual bool Move(bool pausa)
        {
            if (Console.KeyAvailable)
                {

                    ConsoleKeyInfo keya = Console.ReadKey();

                    if (!pausa)
                    {
                        switch (keya.Key)
                        {
                            case ConsoleKey.RightArrow:
                                moveRight();
                                break;
                            case ConsoleKey.LeftArrow:
                                moveLeft();
                                break;
                            case ConsoleKey.UpArrow:
                                moveUp();
                                break;
                            case ConsoleKey.DownArrow:
                                moveDown();
                                break;
                            case ConsoleKey.Spacebar:
                                if (!pausa)
                            {
                                pausa = true;
                            }
                                Console.SetCursorPosition(x, y);
                                break;
                        }
                    }
                    else
                    {
                        if (keya.Key == ConsoleKey.Spacebar)
                        {
                            pausa = false;
                            Console.Beep(500, 100);
                        }
                    }
                }
            return pausa;
        }
        public int getHp()
        {
            return hp;
        }

        public void moveLeft()
        {
            if (x > 0)
            {
                x--;
            }
        }

        public void moveRight()
        {
            if (x < Console.WindowWidth - 1)
            {
                x++;
            }
        }

        public void moveUp()
        {
            if (y > 0)
            {
                y--;
            }
        }

        public void moveDown()
        {
            if (y < Console.WindowHeight - 1)
            {
                y++;
            }
        }

        public void increaseHp(int _hp)
        {
            hp += _hp;
        }

        public void decreaseHp(int _hp)
        {
            hp -= _hp;
        }

        public void moveToRespawn()
        {
            x = Console.WindowWidth / 2;
            y = Console.WindowHeight / 2;
        }

        public void draw()
        {
            Console.Clear();
            Console.SetCursorPosition(x, y);
            Console.Write("@");
        }

    }
}
