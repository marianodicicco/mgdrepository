﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsolePlayerMain
{
    class Enemy
    {
        private int x;
        private int y;
        private Random rand = new Random();

        public Enemy()
        {
        }

        public Enemy(int _x, int _y)
        {
            x = _x;
            y = _y;
        }

        public void draw()
        {
            Console.SetCursorPosition(x, y);
            Console.Write("█");
        }

        public int getX()
        {
            return x;
        }

        public int getY()
        {
            return y;
        }

        public void setX(int _x)
        {
            if (_x > 0 && _x < Console.WindowWidth - 1)
            {
                x = _x;
            }
        }

        public void setY(int _y)
        {
            if (_y > 0 && _y < Console.WindowHeight - 1)
            {
                x = _y;
            }
        }

        public void moveRightDown()
        {
            if (x < Console.WindowWidth - 1 && y < Console.WindowHeight - 1)
            {
                x++;
                y++;
            }
            else if (x == Console.WindowWidth - 1 || y == Console.WindowHeight - 1)
            {
                x = rand.Next(0, Console.WindowWidth - 1);
                y = 0;
            }
        }

        public void moveLeftDown()
        {
            if (x > 0 && y < Console.WindowHeight - 1)
            {
                x--;
                y++;
            }
            else if (x == 0 || y == Console.WindowHeight - 1)
            {
                x = rand.Next(0, Console.WindowWidth - 1);
                y = 0;
            }
        }
    }
}
