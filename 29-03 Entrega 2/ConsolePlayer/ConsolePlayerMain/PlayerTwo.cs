﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsolePlayerMain
{
    class PlayerTwo:Player
    {
        public override bool Move(bool pausa)
        {
            if (Console.KeyAvailable)
            {

                ConsoleKeyInfo keya = Console.ReadKey();

                if (!pausa)
                {
                    switch (keya.Key)
                    {
                        case ConsoleKey.D:
                            moveRight();
                            break;
                        case ConsoleKey.A:
                            moveLeft();
                            break;
                        case ConsoleKey.W:
                            moveUp();
                            break;
                        case ConsoleKey.S:
                            moveDown();
                            break;
                        case ConsoleKey.Spacebar:
                            if (!pausa)
                            {
                                pausa = true;
                            }
                            Console.SetCursorPosition(getX(), getY());
                            break;
                    }
                }
                else
                {
                    if (keya.Key == ConsoleKey.Spacebar)
                    {
                        pausa = false;
                        Console.Beep(500, 100);
                    }
                }
            }
            return pausa;
        }
    }
}
