﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ConsolePlayerMain
{
    public class Program
    {

        static void Main(string[] args)
        {
            PlayState game = new PlayState();
            game.gameStart();
        }
    }

}